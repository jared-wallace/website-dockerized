from django.urls import path
from labs import views

urlpatterns = [
    path('home/', views.lab_home, name='lab_home'),
    path('section/<int:section_id>/', views.section_home, name='section'),
    path('section_schedule/<int:section_id>/', views.schedule, name='section_schedule'),
    path('detail/<int:lab_id>', views.lab_detail, name='lab_detail'),
]
