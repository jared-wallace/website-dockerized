"""jwallace_website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from django.conf import settings
from base import views as base_views
from django.conf.urls.static import static

urlpatterns = [
    re_path(r'^$', base_views.index, name="index"),
    path('admin/', admin.site.urls, name="admin"),
    path('accounts/', include('django.contrib.auth.urls')),
    path('noemi/', base_views.noemi, name="noemi"),
    path('about/', base_views.about, name="about"),
    path('contact/', base_views.contact, name="contact"),
    path('projects/', include('projects.urls')),
    path('labs/', include('labs.urls')),
]

if bool(settings.DEBUG):
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
