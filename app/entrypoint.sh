#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

python manage.py flush --no-input
python manage.py migrate
python manage.py collectstatic --no-input --clear
python manage.py loaddata fixtures/labs.json
python manage.py loaddata fixtures/projects.json
python manage.py createsuperuser2 --username jwallace --password changeme --noinput --email 'blank@email.com'

exec "$@"

