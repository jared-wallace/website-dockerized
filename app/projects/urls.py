from django.urls import re_path
from projects import views as projects_views

urlpatterns = [
    re_path(r'^list/(?P<type>(?:(?:\bpersonal\b)|(?:\bwork\b)|(?:\bschool\b)))/$', projects_views.list, name="list"),
    re_path(r'^detail/(?P<id>\d*)/', projects_views.detail, name="detail"),
    re_path(r'^index/', projects_views.index, name="projects_index"),
]
