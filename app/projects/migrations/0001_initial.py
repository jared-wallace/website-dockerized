# Generated by Django 2.2.6 on 2019-11-27 16:37

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('created_date', models.DateTimeField(default=datetime.datetime.now)),
                ('shortdescription', models.CharField(max_length=200)),
                ('fulldescription', models.TextField(max_length=5000)),
                ('binaryfile', models.FileField(blank=True, null=True, upload_to='projects')),
                ('sourcefile', models.FileField(blank=True, null=True, upload_to='projects')),
                ('language', models.CharField(max_length=20)),
                ('classification', models.CharField(choices=[('work', 'Work'), ('personal', 'Personal'), ('school', 'School')], max_length=8)),
            ],
        ),
        migrations.CreateModel(
            name='Screenshots',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='projects')),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='images', to='projects.Project')),
            ],
        ),
    ]
