from django.contrib import admin
from .models import Contact_Information, Day


class ContactInline(admin.TabularInline):
    model = Day
    extra = 3


class ContactAdmin(admin.ModelAdmin):
    list_display = ('phone', 'office', 'email', 'github', 'bitbucket')
    inlines = (ContactInline,)


admin.site.register(Contact_Information, ContactAdmin)
