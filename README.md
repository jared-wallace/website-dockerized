# Jared Wallace's personal site

## Building and running locally

`docker-compose up -d --build`

will start the server on port 8001. Base database fixtures get loaded for you.

## Running in production

Existing install: `./start.sh`

New install: `./init-letsencrypt.sh && ./start.sh`

## After adding quotes or projects

Make sure you run `docker-compose -f docker-compose.prod.yml exec web python manage.py dumpdata projects > projects.json`
and `docker-compose -f docker-compose.prod.yml exec web python manage.py dumpdata labs > labs.json`.

Replace the existing files in the app/fixtures directory, confirm correctness by running locally, and then commit into
VCS.

## Credits

Dockerization guide from https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/

SSL guide from https://medium.com/@pentacent/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71

Site by Jared Wallace
